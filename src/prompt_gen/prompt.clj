;;;; Prompt generation code
(ns prompt-gen.prompt
  (:require [prompt-gen.defaults :as defaults]))

;;; Prompt structure
(defstruct prompt :chars :setup :setting)

;;; Some supporting functions/macros.
(defn setable?
  "Returns true for a collection which can be transformed into a set and false
  otherwise."
  [collt]
  (if (and (coll? collt)
           (not (map? collt)))
    true
    false))

(defmacro if-setable
  "Evaluate an expression based on whether or not the collection can be made into
  a set."
  ([collt true-expr]
   (if `(setable? ~collt)
     true-expr))
  ([collt true-expr false-expr]
   (if `(setable? ~collt)
     true-expr
     false-expr)))

(defn- random-from-set
  "Grab a pseudo-random item from a set. Provides n items if n provided."
  ([set]
   (rand-nth (seq set)))
  ([set n]
   (loop [list-of-items '()
          input-set set]
     (cond
       (= n (count list-of-items)) list-of-items
       (= input-set (empty input-set)) list-of-items
       :else (let [this (rand-nth (seq input-set))]
               (recur (conj list-of-items this) (disj input-set this)))))))

;;; The actual prompt-generation code.
(defn generate-prompts
  "Generate a list of prompts, given a number of prompts to generate and a seed
  for pseudo-randomness. Can also accept a hashmap of overrides for the default
  characters, settings and setups defined above (use :chars, :settings and :setups,
  respectively)."
  [num-prompts & {:keys [chars-override settings-override setups-override]
                   :or {chars-override defaults/characters
                        setups-override defaults/setups
                        settings-override defaults/settings}}]
   (let [;; Check to make sure the overrides are usable collections for the
         ;; function and convert them to sets if so. If not, use the defaults.
         chars (if-setable chars-override
                           (if (not= nil chars-override)
                             (set chars-override)
                             defaults/characters)
                           defaults/characters)
         setups (if-setable setups-override
                            (if (not= nil setups-override)
                              (set setups-override)
                              defaults/setups)
                            defaults/setups)
         settings (if-setable settings-override
                              (if (not= nil settings-override)
                                (set settings-override)
                                defaults/settings)
                              defaults/settings)]
     (loop [prompts []
            used #{}]
       (let [;; The randomness functions. Moved inside the loop so that they're randomized every loop instead of every function call.
             choose-chara (random-from-set chars)
             choose-charb (random-from-set chars)
             choose-char-pair (set (list choose-chara choose-charb))
             choose-setup (random-from-set setups)
             choose-setting (random-from-set settings)

             char-pair choose-char-pair
             set-pair (set char-pair)
             genned-prompt (struct prompt char-pair choose-setup choose-setting)]
         (cond
           ;; If we have enough prompts now, exit and return the vector of prompts.
           (= num-prompts (count prompts)) prompts

           ;; If the character pair has already been used, recurse and generate a
           ;; new prompt.
           (contains? set-pair char-pair)  (recur prompts used)

           ;; Else, recurse, sticking the prompt in the vector and the character
           ;; pair in the set
           :else                           (recur (conj prompts genned-prompt) (conj used set-pair)))))))
