;;;; The main program.
(ns promptify.core
  (:gen-class)
  (:require [prompt-gen.prompt :as prompt-gen]
            [promptify.helpers :as helpers]
            [clojure.string :as string]
            [promptify.yaml-parse :as yaml]))

;;; Some other helper functions.
;; (defn string-number?
;;   "Check if a sting can be converted to a number."
;;   [string]
;;   (let [s (vec string)]
;;     (nil? (-> s (filter #())))))

;; parse-int courtesy of 'snrobot' on stackexchange
;; http://stackoverflow.com/questions/5621279/in-clojure-how-can-i-convert-a-string-to-a-number
(defn parse-int [s]
  (Integer. (re-find  #"\d+" s)))

;; Do I need this any more? I don't remember. Damn my shitty memory.
(defn num-str? [s]
  (when-not (or (nil? s) (not (string? s)))
      (re-find #"\d+" s)))

;; (defn num-str? [s]
;;   (not (nil? (re-find #"\d+" s))))
                                        ;(not (nil? (re-find #"\d+" s))))

;; Okay. How about this:
;;
;; A dictionary of... hm.  A dictionary of regexes to... no?  That doesn't...
;;
;; Okay.  A vector of vectors where the first item is a regex pattern to match
;; against args and the second is a function to call on a match.  The function
;; should return...  what? I'm thinking... a dictionary, maybe? Yeah.  Or... no.
;; A key-value pair in a list to associate to the dictionary using (apply assoc
;; args argument).  I'll call the dictionary "directives."  When handle-args is
;; done, it'll consult "directives" for its major mode.  By default, it's to
;; generate a prompt.  For now, the only major modes are going to be "help" and
;; two-character prompt generation.  Oh, and "list."  I may add more later.
;;
;; Major mode will be defined by a key :mode, and it will refer to a function
;; that may or may not have a return value.
;;
;; Hm... well, this /does/ work pretty well... but the problem is, I'm not sure
;; how I can make it work with flags that require something extra. I guess I
;; could make it use -c="thing" syntax... yeah, that'll work for now. But...
;; shit. How can I access the 

;; This may be moved to promptify.helpers, but I'm not sure, since it's pretty
;; immediately relevant to splitting args. Anyway... here you go.
(defn match-equal-param
  "Return the part of an argument-string after the equal-sign."
  [string]
  (.substring (re-find #"=.+" string) 1))

;; It's probably not necessary to filter out whitespace.  It may actually be
;; harmful, since the shell should have already split the args up appropriately.
;; Thus, the new version.
;;
;; Even newer old regex: #"=\S+"
;; Newer old regex: #"=[^ ]+"
;; Old regex: #"[^=]+\s."

;;
;; Thought: I could probably simplify my argument handler a lot if I used subs
;; and equality tests plus some subhandler functions instead of the regexy mess
;; I have now.
;;
;; I'm thinking it'd work like this: write two functions, where one is for short
;; options and one is for long ones.  Use the cases below to determine which to
;; call.  Both should take strings.  The handler should pass the current argument
;; to them.  The long option one should check the entire string versus its lookup
;; tables.  The short one should strip off the dash at the front (subs s 1) and
;; iterate over each remaining character, checking them against its lookup table
;; and appending them to its return value appropriately.
;;
;; Note, the cases below should be in this order, roughly, because otherwise
;; the short options are going to override the long ones.
;;
;; (= (subs s 0 2) "--")
;; (= (subs s 0 1) "-")
;;



;;; The help text. This is what --help prints.
(def prompter-help-str
  "promptify: a writing prompt generator written in Clojure.
  Git repository: https://spigo900@bitbucket.org/spigo900/promptify.git

  -h, --help: Print this documentation.
  -l, --list: List available prompt pools.
  -c, --choose: Choose a prompt pool as given by -l or --list.
  -p: Generate pony prompts.
  -P: Generate extended pony prompts (includes some fanon locations).
  -S: Generate Supernatural prompts.")



;;; The callbacks.
(defn help-callback
  "Print the help string and exit."
  [& args]
  (println prompter-help-str))

(defn list-callback
  "Print a list of available prompt sets and exit."
  [& args]
   (let [list (string/join "\n" (helpers/get-available-prompt-pools yaml/yaml-path))]
     (println list)))

(defn prompt-callback
  "Call the appropriate prompt generation function and print the prompt given."
  [& {:keys [get-prompt num-prompts]
           :or {get-prompt "pony"
                num-prompts 1}}]
  (let [masterpool (helpers/get-masterpool yaml/yaml-path)
        pool (get masterpool get-prompt)
        resolved (yaml/resolve-to-overrides pool masterpool)
        re-resolved (seq (reduce into (seq resolved)))
        prompted (apply prompt-gen/generate-prompts num-prompts re-resolved)
        clone-formatted (map (fn [x]
                               (assoc x :chars (helpers/format-clone (:chars x)))) prompted)
        full-formatted (fn [inner-prompt]
                         (string/join "\n"
                                      (map helpers/format-prompt clone-formatted)))]
    (println (full-formatted clone-formatted))))


(def long-parameters {"--help" (list :mode help-callback)
                      "--list" (list :mode list-callback)
                      "--choose=.*" (list :get-prompt promptify.core/match-equal-param :prompt-needs-call true)})

(def short-parameters {"-h" (list :mode help-callback)
                       "-l" (list :mode list-callback)
                       "-c=.*" (list :mode prompt-callback :get-prompt promptify.core/match-equal-param :prompt-needs-call true)
                       "-p" (list :mode prompt-callback :get-prompt "pony")
                       "-P" (list :mode prompt-callback :get-prompt "pony-ext")
                       "-S" (list :mode prompt-callback :get-prompt "supernatural")})

(defn handle-long-option
  "Handle a string containing a long option."
  [string options-map]
  (loop [[cur-opt & rem-opts] (keys options-map)]
    (let [found (when-not (nil? cur-opt) (re-find (re-pattern cur-opt) string))]
          (cond
            (nil? cur-opt)    '()
            (= string cur-opt) (get options-map cur-opt)
            (not (nil? found)) (let [this (re-find #"=.*" string)]
                                 (if (> (count this) 1)
                                   (into (list :get-prompt (subs this 1)) (reverse (get options-map cur-opt)))
                                   (get options-map cur-opt)))
            :else              (recur rem-opts)))))

(defn handle-short-options
  "Handle a string containing at least one short option.  First character should
  be a hyphen/dash."
  [string options-map]
  (if (> (count string) 1)
    (let [s (subs string 1)
          check-options
          (fn [o map]
            (loop [[cur-opt & rem-opts] (keys options-map)]
              (let [actual-cur-opt (subs cur-opt 0 2)
                    actual-o (str "-" o)] 
                (cond
                  (nil? rem-opts) '()
                  (= actual-o actual-cur-opt) (get options-map cur-opt)
                  :else (recur rem-opts)))))]
      (loop [stringy s
             return-list '()]
        (if (> (count stringy) 0)
          (let [cur-str (subs stringy 0 1)
                rest-str (subs stringy 1)
                checked (check-options cur-str options-map)]
            (cond
              (= cur-str "=")          (recur "" (into (list :get-prompt (re-find #".*" rest-str)) (reverse return-list)))
              (not (empty? checked))   (recur rest-str (into return-list (reverse checked)))
              :else                    (recur rest-str return-list)))
          return-list)))
    '()))



;;; Argument handling.
(defn handle-args
  "Reorganize the args into a map, where :get-prompt contains the name of a prompt
  and :mode contains the callback function to call with the results and the final
  map at the end."
  [& args]
  (let [check-assoc-app (fn [map expr arg]
                          (if-not (empty? expr)
                            (apply assoc map expr)
                            (assoc map :errors (conj (get map :errors) (str "Option not recognized: " arg)))))]
    (loop [rem-args args
           args-map {:mode prompt-callback}]
      (let [cur-arg (first rem-args)]
        (cond
          (nil? cur-arg) args-map
          (num-str? cur-arg) (recur (rest rem-args) (assoc args-map :num-prompts (parse-int cur-arg)))
          (= (subs cur-arg 0 2) "--") (recur (rest rem-args) (check-assoc-app args-map (handle-long-option cur-arg long-parameters) cur-arg))
          (= (subs cur-arg 0 1) "-") (recur (rest rem-args) (check-assoc-app args-map (handle-short-options cur-arg short-parameters) cur-arg))
          :else (recur (rest rem-args) args-map))))))



;;; The frontend. Currently works via commandline args.
(defn -main
  [& args]
  (let [args-map (apply handle-args args)
        callback (:mode args-map)
        reduced-to-seq (seq (reduce into (seq args-map)))]
    (apply callback reduced-to-seq)))
