(ns promptify.yaml-parse
  (:require [clj-yaml.core :as yaml]
            [clojure.java.io :as io]
            [clojure.set :as sets]))

;;; Parsing code.
(def yaml-path "resources")
(defn get-yaml-resources
  "Get a list of all the YAML files in the directory.  Returns Java file handles."
  [path]
  (->> (-> path io/file file-seq)
       (filter #(not (.isDirectory %)))
       (filter #(re-find #".+\.yaml" (.getName %)))))

(defn get-data
  "Parse all the YAML files in a directory and return the data structure created."
  [path]
  (->> (map #(-> % slurp yaml/parse-string) (get-yaml-resources path))
       (reduce concat)))

(defn extract-pools-file
  "Take a vector as created by parsing a YAML file and return a vector of all the
  prompt-pool maps described in that file."
  [pools-in-file]
  (loop [unhandled-params pools-in-file
         coll-in-progress '()]
    (cond
      (< (count unhandled-params) 2) coll-in-progress
      :else (if (-> unhandled-params first (= :prompt-pool))
              (recur (drop 2 unhandled-params) (conj coll-in-progress (second unhandled-params)))
              (recur (drop 2 unhandled-params) coll-in-progress)))))


;;; Masterpool functions.
;; Helper-macro for add-to-masterpool.
(defmacro set-correction
  "Checks if the input is a string.  If it is, converts it to a list and then a
  set.  If not, just convert it to a set.  If it's nil, just return an empty set."
  [input#]
  `(if (string? ~input#)
     (set (list ~input#))
     (if-not (nil? ~input#)
       (set ~input#)
       #{})))

;; Add a prompt pool as generated above to a masterpool.
(defn add-to-masterpool
  "Associate a raw prompt pool taken from a YAML file to a masterpool structure."
  [yaml-pool masterpool]
  (let [this-id (:id yaml-pool)]
    (assoc masterpool
           this-id {:inherit (let [inherits (:inherit yaml-pool)]
                               (assoc inherits
                                      :characters (set-correction (:characters inherits))
                                      :setups (set-correction (:setups inherits))
                                      :settings (set-correction (:settings inherits))))
                    :exclude (let [excludes (:exclude yaml-pool)]
                               (assoc excludes
                                      :characters (set-correction (:characters excludes))
                                      :setups (set-correction (:setups excludes))
                                      :settings (set-correction (:settings excludes))))
                    :characters (set (:characters yaml-pool))
                    :setups (set (:setups yaml-pool))
                    :settings (set (:settings yaml-pool))})))

(defn resolve-to-overrides
  "Resolve a prompt pool within a masterpool to character/setting overrides usable
  by generate-prompt."
  [pool masterpool]
  (let [;; Some sanity check code that probably isn't necessary any more.
        is-any-nil?
        (fn [list]
          (reduce (fn [x acc]
                    (if (nil? acc)
                      true
                      (if (nil? x)
                        true
                        false))) false list))

        ;; Should return a masterpool where all top-level keys are those that
        ;; include 'key' in their 'type' key.  Used to resolve inherits and
        ;; excludes below.
        narrow-by-key
        (fn [type key]
          (select-keys masterpool (key (type pool))))

        ;; Used to retrieve and merge the necessary sets for inclusion/exclusion.
        retrieve-set-data
        (fn [type key alt]
          (let [found (->> (narrow-by-key type key)
                           vals
                           (map (fn [x] (get x key alt))))]
            (reduce into #{} found)))
        to-include {:characters (retrieve-set-data :inherit :characters #{"Jebediah Kerman"})
                    :setups (retrieve-set-data :inherit :setups #{"realize they've found a bug"})
                    :settings (retrieve-set-data :inherit :settings #{"in... what is that, even?"})}

        create-exclude-set
        (fn [key]
          (let [exclude-set (-> (:exclude pool) key)]
            (if-not (or (empty? exclude-set) (map? exclude-set))
              (set exclude-set)
              #{})))
        to-exclude {:characters (create-exclude-set :characters)
                    :setups (create-exclude-set :setups)
                    :settings (create-exclude-set :settings)}

        resolve-override
        (fn [key]
          (into
           (set (key pool))
           (sets/difference
            (get to-include key)
            (get to-exclude key))))
        overrides {}

        sanity-check-key
        (fn [key]
          (let [resolved (resolve-override key)]
            (if (empty? resolved)
              nil
              resolved)))]
    (assoc {}
           :chars-override (sanity-check-key :characters)
           :setups-override (sanity-check-key :setups)
           :settings-override (sanity-check-key :settings))))

(defn extract-masterpool
  "Take a list of vectors generated by get-data and extract a masterpool from it."
  [data]
  (->>
   (map extract-pools-file data)
   (reduce into)
   (reduce #(add-to-masterpool %2 %1) {})))
