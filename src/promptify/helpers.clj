;;;; Helper functions for the main program loop.
(ns promptify.helpers
  (:require [prompt-gen.prompt
             :as prompt-gen
             :only prompt-gen.prompt/setable?]
            [promptify.yaml-parse :as yaml]))

;;; Helper functions.
(defn get-masterpool
  "Get a masterpool of prompts from available from the YAML files in a given path."
  [path]
  (-> (yaml/get-data path) yaml/extract-masterpool))

(defn get-available-prompt-pools
  "Get a list of prompt pools available from the YAML files in a given path."
  [path]
  (keys (get-masterpool path)))

;; These functions format the strings presented to the user.
(defn format-clone
  "Check to see if both strings of a pair of character strings are the same.  If
  so, append \"'s clone\" to the end."
  [pair]
  (let [firsty (first pair)]
      (if (= firsty (last pair))
        (list firsty (str firsty "'s clone"))
        pair)))

(defn format-prompt
  "Take a prompt struct and return a formatted string version."
  [prompt]
  (str (first (:chars prompt)) " and " (last (:chars prompt)) " "
       (:setup prompt) " "
       (:setting prompt) "."))

(defn format-prompt-pool
  "Take a list of prompt pool variables and format it as a map for use with
  generate-prompts."
  [chars setups settings]
  (assoc {}
         :chars-override (if (and (not (empty? chars)) (prompt-gen/setable? chars))
                           chars
                           nil)
         :setups-override (if (and (not (empty? setups)) (prompt-gen/setable? setups))
                            setups
                            nil)
         :settings-override (if (and (not (empty? settings)) (prompt-gen/setable? settings))
                              settings
                              nil)))
