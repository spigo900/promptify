# promptify

**promptify** is a commandline writing prompt generator written in Clojure.  Given
a set of possible characters, set-ups and settings (collectively called a *prompt
pool*), it generates a set of random story prompts using two of the characters
(or a character and their clone) and prints them as its output.  Prompt pools are
defined using YAML files in the resource directory.

Currently, prompt pools are included for *My Little Pony: Friendship is Magic*
and *Supernatural* fan fiction.  Custom prompt pools can be written in YAML

## Installation

Download promptify.1.0.0.zip and extract it. For compilation instructions, see
*Compiling* below.

## Usage

Ensure that your jar file is in the same folder as your resources folder.  Open a
terminal or command prompt (`cmd.exe` on Windows), navigate to the folder using cd,
and use `java` to run.  To list the available prompt types use:

    $ java -jar promptify-1.0.0-RELEASE.jar --list

Specify the number and type of prompts to generate as a number on the commandline,
like so, replacing *pool* with the pool you'd like to use as shown by `--list` and
*num* with the number of prompts to generate:

    $ java -jar promptify-1.0.0-RELEASE.jar --choose="pool" num

## Options

  * `-l, --list`: List available prompt pools.

  * `-c="pool", --choose="pool"`: Generate prompts using prompt pool *pool*.

  * `-h, --help`: Print help documentation.

  * `-p`: Equivalent to -c=pony (generates pony prompts).

  * `-P`: Equivalent to -c=pony-ext (generates pony prompts including some fanon
  locations and characters.).

  * `-S`: Generate Supernatural prompts.


## Compiling

Compiling promptify requires the [Clojure compiler](clojure.org) and
[Leiningen](http://leiningen.org/).

Download the source code from https://bitbucket.org/spigo900/promptify and compile
using `lein uberjar`.


### Bugs

None known.


## License

Copyright � 2016 Joseph A. Cecil

Distributed under the 3-clause BSD license.
