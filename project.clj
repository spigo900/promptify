(defproject promptify "1.0.0-SNAPSHOT"
  :description "A fiction-writing prompt generator written in Clojure."
  :url "https://bitbucket.org/spigo900/promptify"
  :license {:name "3-clause BSD License"
            :url "http://opensource.org/licenses/BSD-3-Clause"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [clj-yaml "0.4.0"]]
  :main ^:skip-aot promptify.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
